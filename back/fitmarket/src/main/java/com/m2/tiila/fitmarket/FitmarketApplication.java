package com.m2.tiila.fitmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitmarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(FitmarketApplication.class, args);
	}

}
